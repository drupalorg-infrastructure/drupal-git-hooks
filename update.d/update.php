#!/usr/bin/php
<?php

set_error_handler('handle_error');

require '/etc/drupal-git-hooks/update-settings.php';

// When the request originates from the GitLab API,
// SSL_CERT_DIR=/opt/gitlab/embedded/ssl/certs/ is set. Unset and use the
// system certs so we can make an outside HTTP request.
putenv('SSL_CERT_DIR');

list(, $reftype, $ref) = explode('/', $argv[1], 3);
if (!in_array($reftype, ['heads', 'tags'])) {
  // Deny updating any ref not located in the standard branch/tag locations.
  echo "Drupal.org only allows pushes to standard branch (refs/heads) or tag (refs/tags) locations.\n";
  exit(2);
}

if (!isset($_SERVER['GL_REPOSITORY']) || !isset($_SERVER['GL_PROJECT_PATH'])) {
  // Hopefully this doesn’t happen.
  echo "Unable to determine which repository is being used\n";
  trigger_error('Unable to determine which repository is being used, environment: ' . print_r($_SERVER, TRUE));
  exit(2);
}

$project_id = explode('-', $_SERVER['GL_REPOSITORY'])[1];
$namespace = explode('/', $_SERVER['GL_PROJECT_PATH'])[0];

// Check if this is new or deleted item. If neither, if it is a force push or a
// fast-forward.
if ($argv[2] === '0000000000000000000000000000000000000000') {
  $action = 'new';
}
elseif ($argv[3] === '0000000000000000000000000000000000000000') {
  $action = 'delete';
}
else {
  $result = exec_wrapper('git', ['merge-base', $argv[2], $argv[3]], FALSE);
  if ($result['return'] || $result['output'][0] !== $argv[2]) {
    $action = 'force';
    if (!$result['return']) {
      $merge_base = $result['output'][0];
    }
  }
  else {
    $action = 'ff';
  }
}

if ($namespace === 'issue') {
  // This is an issue fork repository. Do not allow deletion.
  if ($action === 'delete') {
    if ($_SERVER['GL_USERNAME'] !== 'root-admin') {
      echo 'Deleting tags & branches is not allowed for issue fork repositories.' . PHP_EOL;
      exit(2);
    }
  }
  // On force push, tag previous HEAD, so work is not lost.
  if ($action === 'force') {
    $tag = 'previous/' . $ref . '/' . gmdate('Y-m-d');
    $existing_tags = exec_wrapper('git', ['tag', '--list', $tag . '*'])['output'];
    // Check for duplicates.
    if (in_array($tag, $existing_tags)) {
      $n = 1;
      while (in_array($tag . '-' . $n, $existing_tags)) {
        $n += 1;
      }
      $tag .= '-' . $n;
    }
    exec_wrapper('git', ['tag', '-m', ($reftype === 'heads' ? 'Branch' : 'Tag') . ' force pushed by ' . $_SERVER['GL_USERNAME'] . ' at ' . gmdate('c') . ', ' . $ref . ' moved from this tag to ' . $argv[3], $tag, $argv[2]], TRUE, [
      'GIT_COMMITTER_NAME' => 'drupalbot',
      'GIT_COMMITTER_EMAIL' => '2-drupalbot@users.noreply.drupalcode.org',
    ]);
    trigger_error($_SERVER['GL_PROJECT_PATH'] . ': ' . $_SERVER['GL_USERNAME'] . ' force pushed ' . $argv[1] . ' from ' . $argv[2] . ' to ' . $argv[3] . ', backup tagged ' . $tag);
    echo 'The previous HEAD is now tagged ' . $tag . PHP_EOL;
  }
}
elseif ($namespace === 'security') {
  // Private repository for security team use.
}
else {
  // Otherwise, check with Drupal.org.
  $context = stream_context_create(['http' => [
    'method' => 'POST',
    'header' => 'Content-type: application/x-www-form-urlencoded',
  ]]);
  $data = file_get_contents($gitlab_drupal_url . '/drupalorg-gitlab-validate/' . $project_id . '/' . urlencode(urlencode($argv[1])) . '/' . $argv[2] . '/' . $argv[3], FALSE, $context);
  if ($data = json_decode($data)) {
    if ($data !== 'OK') {
      echo $data . "\n";
      exit(2);
    }
  }
  else {
    echo "Unable to contact Drupal.org, please try again soon\n";
    trigger_error('Drupal.org request failed: ' . $data);
    exit(2);
  }
}

function handle_error(int $errno, string $errstr, string $errfile = NULL, int $errline = NULL) {
  $log = '/var/log/gitlab/drupalhook-update.log';
  $logdata = time() . ' ' . $errno . ' ' . $errstr . "\n";
  file_put_contents($log, $logdata, FILE_APPEND | LOCK_EX);
}

function exec_wrapper($command, $arguments = [], $exit_on_nonzero = TRUE, $env_vars = []) {
  $full_command = escapeshellcmd($command) . ' ' . implode(' ', array_map('escapeshellarg', $arguments));
  $pipes = [];
  $process = proc_open($full_command, [1 => ['pipe', 'w'], 2 => ['pipe', 'w']], $pipes, NULL, $env_vars);
  $stdout = stream_get_contents($pipes[1]);
  $stderr = stream_get_contents($pipes[2]);
  fclose($pipes[1]);
  fclose($pipes[2]);
  $return = proc_close($process);

  if ($exit_on_nonzero && $return) {
    echo "Internal server error in Drupal.org Git hooks!" . PHP_EOL;
    trigger_error('exec() exited ' . $return . ': ' . $full_command . PHP_EOL . 'stdout: ' . $stdout . PHP_EOL . 'stderr: ' . $stderr . PHP_EOL . 'Environment: ' . print_r($_SERVER, TRUE));
    exit(2);
  }

  return ['output' => explode(PHP_EOL, $stdout), 'return' => $return];
}
