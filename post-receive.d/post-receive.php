#!/usr/bin/php
<?php

set_error_handler('handle_error');

require '/etc/drupal-git-hooks/post-receive-settings.php';

// When the request originates from the GitLab API,
// SSL_CERT_DIR=/opt/gitlab/embedded/ssl/certs/ is set. Unset and use the
// system certs so we can make an outside HTTP request.
putenv('SSL_CERT_DIR');

$namespace = explode('/', $_SERVER['GL_PROJECT_PATH'])[0];

if ($namespace === 'issue') {
  $payload = json_encode([
    'git_username' => $_SERVER['GL_USERNAME'],
    'repo_id' => explode('-', $_SERVER['GL_REPOSITORY'])[1],
    'data' => file_get_contents('php://stdin'),
    'timestamp' => time(),
  ]);
  $response = file_get_contents($gitlab_drupal_url . '/drupalorg-issue-fork-post-receive', FALSE, stream_context_create([
    'http' => [
      'method' => 'POST',
      'header' => 'Content-type: application/x-www-form-urlencoded',
      'content' => http_build_query([
        'payload' => $payload,
        'hash' => sha1($gitlab_push_key . $payload),
      ]),
    ],
  ]));
  trigger_error(($response === FALSE ? 'failed' : 'success') . ' ' . $payload);
}

function handle_error(int $errno, string $errstr, string $errfile = NULL, int $errline = NULL) {
  $log = '/var/log/gitlab/drupalhook.log';
  $logdata = time() . ' ' . $errno . ' ' . $errstr . "\n";
  file_put_contents($log, $logdata, FILE_APPEND | LOCK_EX);
}
